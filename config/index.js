const path = require('path');
const result = require( 'dotenv' ).config( { path: path.join(__dirname, '..', 'env', '.env') } )
if ( result.error ) {
    throw result.error
}
module.exports = result;