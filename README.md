# Wallex Test Project
_Alireza zeynali_

This the test project for wallex company

This application use two kind of methods to get Ethereum balance and transaction history.
As you know, Ethereum network doesn't have any api for getting accounts transactions history because of that we should 
iterate all ethereum blockchain and check if blocks transactions orginator/destination are equal to our address but
Ethereum blockchain is 184 GB and if we want to get all of it from network it's not optimal.

The best way is to get once and store it on an **Elasticsearch db** and every time update it.

Our example because I don't have enough storage capacity, I decided to ask user to specify the start of search and end of it.

For better experiment I presented another option which use Etherscan Api to get transaction history.

For running this application, in your **CMD** go to application directory after that enter `node index.js`. 
Pay attention you should run `npm install` to install application dependency.After that choose your method if you choose JSONRPC method you can specify your start block and your end block.