const Web3 = require( 'web3' );
const env = require( './config' );
const { TransactionETHERSCAN, BalanceETHERSCAN, BalanceJSONRPC, BlockJSONRPC, TransactionJSONRPC } = require( './classes' );
const inquirer = require( 'inquirer' );

async function Etherscan (){
    let Bal = new BalanceETHERSCAN();
    await Bal.get();
    await Bal.display();
    await console.log( "BLOCK NUMBER ------------ FROM ----------------------------- TO ------------------------------- VALUE" );
    let Tr = new TransactionETHERSCAN();
    await Tr.get();
    await Tr.display();
}

async function JsonRPC (fromBlock, toBlock){
    let Bal = await new BalanceJSONRPC();
    await Bal.get();
    await console.log( "BLOCK NUMBER ------------ FROM ----------------------------- TO ------------------------------- VALUE" );
    let Tr = await new TransactionJSONRPC();
    let Bl = await new BlockJSONRPC( Tr, fromBlock, toBlock );
    if(!toBlock){
        await Bl.current_block();
    }
    await Bl.run()
}

inquirer.prompt( [{
    name: "Method",
    message: "Choose your method",
    type: "list",
    choices: ["ETHERSCAN", new inquirer.Separator(), "JSONRPC"]
}] ).then( async (answer) => {
    if ( answer.Method === "ETHERSCAN" ) {
        await Etherscan();
    } else if ( answer.Method === "JSONRPC" ) {
        await console.log( "This process take long time to finish" );
        inquirer.prompt( [{
            name: "FromBlock",
            message: "input your start block",
            type: "input"
        }] ).then( async (answer) => {
            var FromBlock = answer.FromBlock;
            inquirer.prompt( [{
                name: "ToBlock",
                message: "input your end block",
                type: "input"
            }] ).then(async (answer) => {
                await JsonRPC( parseInt( FromBlock ), parseInt(answer.ToBlock) );

            })


        } ).catch( error => {
            console.error( error )
        } )
    } else {
        throw new Error( "Choose correct method" )
    }
} ).catch( error => {
    console.error( error )
} )


