const Web3 = require( 'web3' );
const chalk = require( 'chalk' );
const Source = require( '../src' );
const { CURRENT_BLOCK_NOT_ACCESSABLE, APICALL_FAILED, BALANCE_NOT_FOUND } = require( '../errors' );
const web3 = new Web3( new Web3.providers.HttpProvider( `https://mainnet.infura.io/v3/${process.env.INFURA_PROJECT_ID}` ) );

class Transaction {
    constructor (){
        this.trxs = [];
    }

    async find (block){
        await block.transactions.forEach( trx => {
            if ( trx.from === process.env.ETHEREUM_ACCOUNT ) {
                console.log( chalk.yellowBright( trx.blockNumber ), chalk.magentaBright( trx.from ), chalk.cyanBright( trx.to ), chalk.blue('OUT'), chalk.redBright(  web3
                    .utils.fromWei(trx.value, "ether") ), 'ETH' );
            } else if ( trx.to === process.env.ETHEREUM_ACCOUNT ) {
                console.log( chalk.yellowBright( trx.blockNumber ), chalk.magentaBright( trx.from ), chalk.cyanBright( trx.to ), chalk.blue('IN'), chalk.redBright( web3
                    .utils.fromWei(trx.value, "ether") ), 'ETH' );
            }
        } )
    }
}

class Block extends Source {
    constructor (transactionObj, fromBlock, toBlock){
        super();
        this.blocks = [];
        if ( fromBlock ) {
            this.fromBlock = fromBlock;
        }
        if ( toBlock ) {
            this.toBlock = toBlock;
        }
        this.transaction = transactionObj;
        this.res = []
    }

    async current_block (){
        await web3.eth.getBlockNumber( async (error, blockNumber) => {
            if ( error ) {
                throw new CURRENT_BLOCK_NOT_ACCESSABLE;
            } else {
                this.toBlock = blockNumber;
            }
        } );
    }

    async run (){
        for (let i = this.toBlock; i > this.fromBlock; i = i - 5) {
            this.res = await Promise.all( [
                web3.eth.getBlock( i, true ),
                web3.eth.getBlock( i - 1, true ),
                web3.eth.getBlock( i - 2, true ),
                web3.eth.getBlock( i - 3, true ),
                web3.eth.getBlock( i - 4, true )
            ] )

            this.res.forEach( (block) => {
                this.transaction.find( block )
            } )

        }
    }
}

class Balance {
    constructor (){
        this.account = process.env.ETHEREUM_ACCOUNT;
    }

    async get (){
        await web3.eth.getBalance( this.account, async (error, balance) => {
            if ( error ) {
                throw new BALANCE_NOT_FOUND;
            } else {
                await console.log( chalk.bgMagentaBright.bold( "BALANCE:" ), chalk.red( web3
                    .utils.fromWei(balance) ), 'ETH' );
            }
        } )
    }
}


module.exports = {
    Balance,
    Block,
    Transaction
}

