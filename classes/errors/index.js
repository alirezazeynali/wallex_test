class APICALL_FAILED extends Error{
    constructor (message){
        super(message || "Api call failed because of unkown problem!!!");
        this.name = "APICALL_FAILED";
    }
}

class TRX_NOT_FOUND extends Error{
    constructor (message){
        super(message || "Transaction not found!!!");
        this.name = "TRX_NOT_FOUND"
    }
}

class BALANCE_NOT_FOUND extends Error{
    constructor (message){
        super(message || "Balance not found!!!");
        this.name = "BALANCE_NOT_FOUND";
    }
}

class CURRENT_BLOCK_NOT_ACCESSABLE extends Error{
    constructor (message){
        super(message||"Current block is not accessable on chain!!!");
        this.name = CURRENT_BLOCK_NOT_ACCESSABLE
    }
}

module.exports = {
    APICALL_FAILED,
    TRX_NOT_FOUND,
    BALANCE_NOT_FOUND,
    CURRENT_BLOCK_NOT_ACCESSABLE
}