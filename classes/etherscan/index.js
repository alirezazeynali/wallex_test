const chalk = require('chalk');
const axios = require('axios');
const Source = require('../src');
const Web3 = require('web3')
const {APICALL_FAILED, TRX_NOT_FOUND, BALANCE_NOT_FOUND} = require('../errors');

class Transaction extends Source{
    async get(){
        try{
            this.trxs = (await axios.get( `http://api.etherscan.io/api?module=account&action=txlist&address=${process.env.ETHEREUM_ACCOUNT}&startblock=${this.fromBlock}&endblock=${this.toBlock}&sort=asc&apikey=${process.env.ETHERSCAN_API_KEY}` )).data.result

        }catch ( e ){
            throw new APICALL_FAILED;
        }
    }
    async display(){
        try{
            for (let i = 0; i<this.trxs.length; i++){
                if(this.trxs[i].from === '0x205e71e60753bab48b9303d3add9a3818e9a5f2b'){
                    console.log(chalk.yellowBright(this.trxs[i].blockNumber), chalk.magentaBright(this.trxs[i].from), chalk.cyanBright(this.trxs[i].to), chalk.blue('OUT'),chalk.redBright(Web3.utils.fromWei(this.trxs[i].value, "ether")), 'ETH');
                }
                if(this.trxs[i].to === '0x205e71e60753bab48b9303d3add9a3818e9a5f2b'){
                    console.log(chalk.yellowBright(this.trxs[i].blockNumber), chalk.magentaBright(this.trxs[i].from), chalk.cyanBright(this.trxs[i].to), chalk.blue('IN'),chalk.redBright(Web3.utils.fromWei(this.trxs[i].value, "ether")), 'ETH');
                }
            }
        }catch ( e ){
            console.log(e)
            throw new TRX_NOT_FOUND;
        }
    }
}
class Balance{
    async get(){
        try{
            this.balance = (await axios.get( `https://api.etherscan.io/api?module=account&action=balance&address=${process.env.ETHEREUM_ACCOUNT}&tag=latest&apikey=${process.env.ETHERSCAN_API_KEY}` )).data.result
        }catch ( e ){
            throw new APICALL_FAILED;
        }
    }
    async display(){
        try{
            console.log(chalk.bgMagentaBright.bold("BALANCE:"), chalk.red(Web3.utils.fromWei(this.balance)), 'ETH');
        }catch ( e ){
            throw new BALANCE_NOT_FOUND
        }
    }
}


module.exports = {Transaction, Balance}