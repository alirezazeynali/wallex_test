class Source {
    constructor (fromBlock, toBlock){
        if ( fromBlock === undefined ) {
            this.fromBlock = 0;
        } else {
            this.fromBlock = fromBlock;
        }
        if ( toBlock === undefined ) {
            this.toBlock = 99999999;
        } else {
            this.toBlock = toBlock;
        }
    }
}

module.exports = Source;