const TransactionETHERSCAN = require('./etherscan').Transaction;
const BalanceETHERSCAN = require('./etherscan').Balance;
const TransactionJSONRPC = require('./jsonRpc').Transaction;
const BalanceJSONRPC = require('./jsonRpc').Balance;
const BlockJSONRPC = require('./jsonRpc').Block;

module.exports = {
    TransactionETHERSCAN,
    BalanceETHERSCAN,
    TransactionJSONRPC,
    BlockJSONRPC,
    BalanceJSONRPC
};